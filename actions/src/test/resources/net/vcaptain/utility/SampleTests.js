describe("sample", function() {
    var sample = System.getModule("net.vcaptain.utility.configuration").sample;
    it("should add two numbers", function() {
        expect(sample(5, 2)).toBe(7);
    });
});