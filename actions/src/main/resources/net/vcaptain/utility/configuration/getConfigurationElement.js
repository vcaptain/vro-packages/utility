/**
 * retrieves a specified configuration element
 * @author TNavarro
 * @function getConfigurationElement
 * @param {string} configurationCategoryPath 
 * @param {string} configurationName
 * @returns {configurationElement}
 */
(function(configurationCategoryPath, configurationName) {
    var category = Server.getConfigurationElementCategoryWithPath(configurationCategoryPath);
    if (!category) {
        throw "Missing configuration element category " + configurationCategoryPath;
    }
    for (var idx = 0; idx < category.configurationElements.length; idx++) {
        if (category.configurationElements[idx].name === configurationName) {
            return category.configurationElements[idx];
        }
    }
    System.warn("cannot find " + configurationCategoryPath + "/" + configurationName + "configuration element");
    return null;
})