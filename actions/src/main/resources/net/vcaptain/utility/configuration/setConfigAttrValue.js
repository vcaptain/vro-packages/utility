/**
 * retrieves a specified configuration element
 * @author TNavarro
 * @function setConfigAttrValue
 * @param {string} configurationCategoryPath -
 * @param {string} configurationName -
 * @param {string} attributeName
 * @param {any} attributeValue
 * @returns {void} 
 */
(function(configurationCategoryPath, configurationName, attributeName, attributeValue) {
    var category = Server.getConfigurationElementCategoryWithPath(configurationCategoryPath);
    if (!category) {
        throw "Missing configuration element category " + configurationCategoryPath;
    }
    for (var idx = 0; idx < category.configurationElements.length; idx++) {
        if (category.configurationElements[idx].name === configurationName) {
            return category.configurationElements[idx];
        }
    }
    System.warn("cannot find zzz" + configurationCategoryPath + "/" + configurationName + "configuration element");
    return null;
})