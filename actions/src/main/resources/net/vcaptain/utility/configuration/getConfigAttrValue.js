/**
 * retrieves the value of an attribute defined ina configuraiton element
 * @author TNavarro
 * @function getConfigAttrValue
 * @param {string} configurationCategoryPath -
 * @param {string} configurationName -
 * @param {string} attributeName
 * @returns {any} 
 * 
 */
(function(configurationCategoryPath, configurationName, attributeName) {
    // copied from GitHub gist
    var attribute;
    var configElement = System.getModule("net.vcaptain.utility.configuration").getConfigurationElement(configurationCategoryPath, configurationName);
    if (!configElement) {
        throw "Missing configuration element " + configurationCategoryPath + "/" + configurationName;
    }
    attribute = configElement.getAttributeWithKey(attributeName);
    if (attribute) {
        return attribute.value;
    }
    System.warn("Cannot find attibute " + attribute + " in " + configurationCategoryPath + "/" + configurationName + " configuration element");
    return null;
})