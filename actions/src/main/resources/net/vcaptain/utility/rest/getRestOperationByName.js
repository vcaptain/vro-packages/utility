/**
 * Given a RESTHOST (objecT) and a restoperation name (string) returns the Rest Operation object(REST:RESTOperation)
 * @author Carlos Tronco (cars@vcaptain.net)
 * @function getRestOperationByName
 * @param {REST:RESTHost} restHost 
 * @param {string} restOperationName
 * @returns {REST:RESTOperation} 
 */
(function(restHost, restOperationName) {
    System.debug("starting Action:getRestOperationByName");
    System.debug("Input: restHost = [" + JSON.stringify(restHost) + "]");
    System.debug("Input: restOperationName = [" + restOperationName + "]");
    var arrRestOperations;
    var restOperation;
    for (var idx = 0; idx < arrRestOperations.length; idx++) {
        restOperation = restHost.getOperation(arrRestOperations[idx]);
        if (restOperation.name.toLowerCase() == restOperationName.toLowerCase()) {
            return restOperation
        }
    }
    System.error("Could not find a RESTOperation iwth name: [" + restOperationName + "] on host " + restHost.name);
    throw ("Could not find a RESTOperation iwth name: [" + restOperationName + "] on host " + restHost.name);
})