/**
 * Given a RestHost name and RestOperation name returns the RESTOperation object
 * @author Carlos Tronco (cars@vcaptain.net)
 * @function getRestOperation
 * @param {string} restHostName
 * @param {string} restOperationName
 * @returns {REST:RESTOperation} - Returns the rest op object
 */
(function(restHostName, restOperationName) {
    System.debug("Starting Action:getRestOperation");
    System.debug("Input: restHostName= [" + restHostName + "]");
    System.debug("Input: restOperationName = [" + restOperationName + "]");

    var restHost = System.getModule("net.vcaptain.utility.rest").getRestHostByName(restHostName);
    var restOperation = System.getModule("net.vcaptain.utility.rest").getRestOperationByName(restHostName, restOperationName);
    if ((restHost == null) || (restOperation == null)) {
        throw "Error getting Rest Operation Information";
    }
    return restOperation;
})