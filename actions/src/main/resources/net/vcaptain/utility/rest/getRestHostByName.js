/**
 * Returns a RESTHost object based on the name
 * @author Carlos Tronco (cars@vcaptain.net)
 * @function getRestHostByName
 * @param {string} restHostName - String value representing name of the rest host in inventory
 * @returns {REST:RESTHost} - returns the rest host object
 */
(function(restHostName) {
    System.debug("starting Action:getRestHostByName");
    System.debug("Input: restHostName = [" + restHostName + "]");

    var arrRestHosts
    var restHost
    var idx;
    arrRestHosts = RESTHostManager.getHosts();
    for (idx = 0; idx < arrRestHosts.length; idx++) {
        restHost = RESTHostManager.getHost(arrRestHosts[idx]);
        if (restHost.name.toLowerCase() == restHostName.toLowerCase()) {
            return restHost;
        }
    }
    System.error("getRestHostByName - Couldn't find a RESTHost with name " + restHostName);
    throw ("getRestHostByName - Couldn't find a RESTHost with name " + restHostName);
})